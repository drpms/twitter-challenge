import { expect } from "chai";
import { shallowMount } from "@vue/test-utils";
import Stream from "@/components/Stream.vue";
import Tweet from "@/components/Tweet.vue";

describe("Stream.vue", () => {
  it("Loads the correct number of tweets", () => {
    const count = 0;
    const wrapper = shallowMount(Stream, {
      propsData: { count }
    });
    expect(wrapper.findAll(Tweet)).to.have.lengthOf(count);
  });
});
