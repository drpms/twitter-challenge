import { expect } from "chai";
import { shallowMount } from "@vue/test-utils";
import EditForm from "@/components/EditForm.vue";

class LocalStorageMock {
  constructor() {
    this.store = {};
  }

  clear() {
    this.store = {};
  }

  getItem(key) {
    return this.store[key] || null;
  }

  setItem(key, value) {
    this.store[key] = value.toString();
  }

  removeItem(key) {
    delete this.store[key];
  }
}

describe("EditForm Unit Test", () => {
  global.localStorage = new LocalStorageMock();

  it("Sets the correct configuration values", () => {
    const sortBy = "Most recent";
    const tweetsForMakeSchool = 3;
    const tweetsForHackerNews = 3;
    const tweetsForYCombinator = 3;
    const wrapper = shallowMount(EditForm, {
      propsData: {
        sortBy,
        tweetsForMakeSchool,
        tweetsForHackerNews,
        tweetsForYCombinator
      }
    });

    const select = wrapper.find("select").element;
    expect(select.value).to.include(sortBy);

    var value1 = wrapper.find("#tweetsForMakeSchool").attributes().value;
    expect(Number(value1)).to.equal(tweetsForMakeSchool);

    var value2 = wrapper.find("#tweetsForMakeSchool").attributes().value;
    expect(Number(value2)).to.equal(tweetsForMakeSchool);

    var value3 = wrapper.find("#tweetsForMakeSchool").attributes().value;
    expect(Number(value3)).to.equal(tweetsForMakeSchool);
  });
});
