import { expect } from "chai";
import { shallowMount } from "@vue/test-utils";
import Tweet from "@/components/Tweet.vue";

describe("Tweet.vue", () => {
  it("Renders the tweet content", () => {
    const tweetContent = "new tweet";
    const createdAt = Date();
    const userUrl = "url";
    const username = "username";
    const name = "name";
    const tweetLink = "https://twitter.com";
    const tweetId = "123123123";
    const formatedDate = "20181212";
    const retweetedCount = "4";
    const retweetedFrom = {
      user: {
        screen_name: "user"
      }
    };
    const userMentions = [
      {
        screen_name: "user"
      }
    ];

    const wrapper = shallowMount(Tweet, {
      propsData: {
        tweetContent,
        createdAt,
        userUrl,
        username,
        name,
        tweetLink,
        tweetId,
        formatedDate,
        retweetedCount,
        retweetedFrom,
        userMentions
      }
    });
    expect(wrapper.text()).to.include(tweetContent);
  });
});
